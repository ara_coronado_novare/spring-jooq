package hk.com.novare.springjooq.http.service.impl;

import hk.com.novare.springjooq.http.service.HttpRestService;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public final class HttpRestServiceImpl implements HttpRestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpRestServiceImpl.class);
    private final RestTemplate restTemplate;

    @Autowired
    public HttpRestServiceImpl(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public <T> Flowable<T> exchangeForBody(final RequestEntity<T> requestEntity, final Class<T> clazz) {
        return Flowable.fromCallable(
            () -> {
                LOGGER.info("Sending HTTP Request: {}", requestEntity);
                return this.restTemplate.exchange(requestEntity, clazz).getBody();
            }
        );
    }

    @Override
    public <T> Flowable<T> exchangeForBody(
        final RequestEntity<T> requestEntity,
        final ParameterizedTypeReference<T> typeRef
    ) {
        return Flowable.fromCallable(
            () -> {
                LOGGER.info("Sending HTTP Request: {}", requestEntity);
                return this.restTemplate.exchange(requestEntity, typeRef).getBody();
            }
        );
    }

    @Override
    public <T> Flowable<ResponseEntity<T>> exchange(final RequestEntity<T> requestEntity, final Class<T> clazz) {
        return Flowable.fromCallable(
            () -> {
                LOGGER.info("Sending HTTP Request: {}", requestEntity);
                return this.restTemplate.exchange(requestEntity, clazz);
            }
        );
    }

    @Override
    public <T> Flowable<ResponseEntity<T>> exchange(
        final RequestEntity<T> requestEntity,
        final ParameterizedTypeReference<T> typeRef
    ) {
        return Flowable.fromCallable(
            () -> {
                LOGGER.info("Sending HTTP Request: {}", requestEntity);
                return this.restTemplate.exchange(requestEntity, typeRef);
            }
        );
    }
}
