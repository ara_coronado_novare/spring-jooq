package archunit;

import archunit.common.ScopeClasses;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaMethod;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import com.tngtech.archunit.lang.syntax.elements.GivenClassesConjunction;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.junit.Test;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Parameter;

public class ControllerTest {
    private final Class[] restAnnotations = new Class[] {
        GetMapping.class,
        PostMapping.class,
        PutMapping.class,
        DeleteMapping.class,
        RequestMapping.class
    };

    private final Class[] parameterAnnotations = new Class[] {
        RequestParam.class,
        RequestHeader.class,
        RequestBody.class,
        PathVariable.class
    };

    private static GivenClassesConjunction controllers() {
        return ArchRuleDefinition.classes()
            .that()
            .areAnnotatedWith(RestController.class);
    }

    @Test
    public void naming() {
        ControllerTest.controllers()
            .should()
            .haveNameMatching(".*(Controller)")
            .check(ScopeClasses.CLASSES);
    }

    @Test
    public void packageLocation() {
        ControllerTest.controllers()
            .should()
            .resideInAPackage("..controller..")
            .check(ScopeClasses.CLASSES);
    }

    @Test
    public void finalFields() {
        ControllerTest.controllers()
            .should()
            .haveOnlyFinalFields()
            .check(ScopeClasses.CLASSES);
    }

    @Test
    public void hasSwaggerAnnotations() {
        final ArchCondition<JavaClass> annotatedBySwagger =
            new ArchCondition<JavaClass>("must be annotated by Swagger Annotations") {
                @Override
                public void check(final JavaClass javaClass, final ConditionEvents events) {
                    for (final JavaMethod method : javaClass.getMethods()) {
                        final Boolean isRestEndpoint = methodIsRestEndpoint(method);
                        if (isRestEndpoint && !method.isAnnotatedWith(ApiOperation.class)) {
                            handleMethod(events, method);
                        }
                        if (isRestEndpoint) {
                            handleParameter(events, method);
                        }
                    }
                }

            };
        ControllerTest.controllers()
            .should(annotatedBySwagger)
            .check(ScopeClasses.CLASSES);
    }

    private void handleMethod(final ConditionEvents events, final JavaMethod method) {
        final String message = String.format(
            "Method %s not annotated by @ApiOperation",
            method.getFullName()
        );
        events.add(SimpleConditionEvent.violated(method, message));
    }

    private void handleParameter(final ConditionEvents events, final JavaMethod method) {
        for (final Parameter parameter : method.reflect().getParameters()) {
            if (parameterIsRestParam(parameter) && !parameter.isAnnotationPresent(ApiParam.class)) {
                final String message = String.format(
                    "Parameter [%s] from method [%s] not annotated by @ApiParam",
                    parameter.getName(),
                    method.getFullName()
                );
                events.add(SimpleConditionEvent.violated(parameter, message));
            }
        }
    }

    private Boolean methodIsRestEndpoint(final JavaMethod method) {
        for (final Class annotation : this.restAnnotations) {
            if (method.isAnnotatedWith(annotation)) {
                return true;
            }
        }
        return false;
    }

    private Boolean parameterIsRestParam(final Parameter parameter) {
        for (final Class annotation : this.parameterAnnotations) {
            if (parameter.isAnnotationPresent(annotation)) {
                return true;
            }
        }
        return false;
    }
}
